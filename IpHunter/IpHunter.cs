﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace IpHunter
{
   
    class IpHunter
    {
        public IpHunter() {
            
        }

        public void StartListen() {
            UdpClient udpClient = new UdpClient();
            IPEndPoint groupEP = null;
            try
            {
                byte[] temp = Encoding.UTF8.GetBytes("whereAreYou?");
                int count = udpClient.Send(temp, temp.Length, "255.255.255.255", 8888);
                Console.WriteLine("Поиск серверов...");
                while (true)
                {
                    byte[] bytes = udpClient.Receive(ref groupEP);
                    if (bytes != null)
                    {
                        Console.WriteLine($"С адреса {groupEP} :");
                        Console.WriteLine($"поступил ответ: {Encoding.UTF8.GetString(bytes, 0, bytes.Length)}");
                        break;
                    }
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                udpClient.Close();
            }




        }

    }
}
