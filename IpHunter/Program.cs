﻿using System;

namespace IpHunter
{
    class Program
    {
        static void Main(string[] args)
        {
            IpHunter hunter = new IpHunter();
            hunter.StartListen();

            Console.ReadLine();
        }
    }
}
