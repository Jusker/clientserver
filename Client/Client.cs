﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;

namespace Client
{
    
    class Client : IDisposable
    {
        private TcpClient tcpClient = null;
        public Client() {
            tcpClient = new TcpClient();
        }
        public void ConnectToServer(string host, int port) {
            tcpClient.Connect(host, port);
            byte[] bytes = new byte[256];
            while (true) {
                var data = Encoding.UTF8.GetBytes(Console.ReadLine());
                var stream = tcpClient.GetStream();

                stream.Write(data, 0, data.Length);
                stream.Flush();


                data = null;

                string result = "";

                int i;

                try
                {
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        result = Encoding.UTF8.GetString(bytes, 0, i);

                        Console.WriteLine("С сервера пришло сообщение: " + result);
                    }

                }
                catch {

                }
                finally
                {
                    stream.Close();

                }
            }
            

            
        }

        public void Dispose()
        {
            if (tcpClient != null)
                tcpClient.Close();
        }
    }
}
