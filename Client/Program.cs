﻿using System;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            using (Client client = new Client()) {
                client.ConnectToServer("127.0.0.1", 8888);

            }
        }
    }
}
