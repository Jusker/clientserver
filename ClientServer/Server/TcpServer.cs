﻿using System.Text.Json;
using Server.Model;
using System;
using System.Diagnostics;

using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Server.Service
{
    class TcpServer : IDisposable
    {
        private TcpListener tcpListener = null;
        private bool IsRunning { get; set; } = true;

        public TcpServer(string host, int port) {
            IPAddress ip = IPAddress.Parse(host);
            tcpListener = new TcpListener(ip, port);
            tcpListener.Start();
            Debug.WriteLine("Start completed");
            
        }

        public void StartServer() {
            Console.WriteLine("Сервер стартовал");
            while (IsRunning) {
                if (tcpListener.Pending())
                {
                    TcpClient newClient = tcpListener.AcceptTcpClient();

                    Thread thread = new Thread(new ParameterizedThreadStart((obj) =>
                    {
                        byte[] bytes = new byte[256];
                        string data = null;

                        using (TcpClient client = (TcpClient)obj)
                        {
                            Console.WriteLine("User вошел в чат");


                            data = null;

                            //Получение потока для связи с клиентом
                            NetworkStream stream = client.GetStream();
                            //заглушка, чтобы очистить буфер 
                            //client.GetStream().Read(bytes, 0, bytes.Length);


                            int i;

                            try
                            {
                                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                                {
                                    data = Encoding.UTF8.GetString(bytes, 0, i);

                                    Console.WriteLine("Client > " + data);
                                    //Обработка сообщения ->
                                    //Приведение к JSON'у
                                    //Вычисление операции

                                    string result = "";
                                    var a = JsonSerializer.Deserialize<UserAction>(data);
                                    switch (a.ActionType) {
                                        case ActionType.Exit:
                                            Environment.Exit(0);
                                            break;
                                        case ActionType.Factorial:
                                             
                                            break;
                                        case ActionType.Minus:
                                            result = $"{a.One} - {a.Two} = "+(a.One - a.Two).ToString();
                                            break;
                                        case ActionType.Multiplication:
                                            result = $"{a.One} * {a.Two} = " + (a.One * a.Two).ToString();
                                            break;
                                        case ActionType.Sum:
                                            result = $"{a.One} + {a.Two} = " + (a.One + a.Two).ToString();
                                            break;
                                        default:
                                            result = "Incorrect oper type";
                                            break;

                                    }
                                    stream.Write(Encoding.UTF8.GetBytes(result),0, Encoding.UTF8.GetBytes(result).Length);
                                    stream.Flush();


                                    // Возвращение результата

                                }
                            }
                            catch
                            {

                            }
                            finally
                            {
                                stream.Close();
                            }
                            Console.WriteLine("User is disconnecting");
                        }

                    }));
                    thread.Start(newClient);
                }
            }
        }

        

        public void Dispose()
        {
            tcpListener.Stop();
            Debug.WriteLine("Stop completed");
        }
    }
}
