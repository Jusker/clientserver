﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Server.Server
{
    class UdpServer: IDisposable
    {

        private UdpClient udpClient = null;

        public UdpServer(int portUdp = 8888) {
            udpClient = new UdpClient(portUdp);

        }

        public void StartUpdReceivier()
        {
            Debug.WriteLine("Start UDP server");
            IPEndPoint groupEP = null;

            while (true)
            {
                byte[] bytes = udpClient.Receive(ref groupEP);
                if (bytes != null)
                {
                    Console.WriteLine($"Сообщение от {groupEP} : {Encoding.UTF8.GetString(bytes, 0, bytes.Length)}");

                    Console.WriteLine("Подготавливаем ответ...");
                    byte[] temp = Encoding.UTF8.GetBytes("IAmHere!");

                    //udpClient.Connect(groupEP);
                    int count = udpClient.Send(temp, temp.Length, groupEP);
                    Console.WriteLine("Ответ отправлен");

                }
            }
        }

        public void Dispose()
        {
            if (udpClient != null)
                udpClient.Close();
            Debug.WriteLine("Завершение");
        }

    }
}
