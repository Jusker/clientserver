﻿
namespace Server.Model
{
    public enum ActionType {
        Sum = 0,
        Minus = 1,
        Multiplication = 2,
        Factorial = 3,
        Exit = 4
    };

    public class UserAction
    {
        public ActionType ActionType { get; set; }
        public decimal One { get; set; }
        public decimal Two { get; set; }
    }
}
