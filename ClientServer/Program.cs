﻿using Server.Server;
using Server.Service;
using System;
using System.Threading;

namespace ClientServer
{
    class Program
    {
        static void Main(string[] args)
        {

            Thread udpThread = new Thread(new ParameterizedThreadStart((obj) =>
            {
                using (UdpServer server = new UdpServer())
                {
                    server.StartUpdReceivier();
                    
                }

            }));
            udpThread.IsBackground = true;
            udpThread.Start();
            
            using (TcpServer server = new TcpServer("127.0.0.1", 8888))
            {
                server.StartServer();
            }



            

        }
    }
}
